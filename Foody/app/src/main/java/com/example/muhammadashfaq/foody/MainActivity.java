package com.example.muhammadashfaq.foody;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ImageView imgVuBurger,imgVuFries,imgVuPrathaRoll,imgVuSandwich;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imgVuBurger=findViewById(R.id.img_vu_burger);
        imgVuFries=findViewById(R.id.img_vu_fries);
        imgVuPrathaRoll=findViewById(R.id.img_vu_paratharoll);
        imgVuSandwich=findViewById(R.id.img_vu_sandwich);

        imgVuBurger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,OrderActivity.class);
                intent.putExtra("FoodName","Burger");
                intent.putExtra("Price","RS200");
                startActivity(intent);
            }
        });
        imgVuFries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,OrderActivity.class);
                intent.putExtra("FoodName","Fries");
                intent.putExtra("Price","RS150");
                startActivity(intent);
            }
        });
        imgVuPrathaRoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,OrderActivity.class);
                intent.putExtra("FoodName","Paratha Roll");
                intent.putExtra("Price","RS100");
                startActivity(intent);
            }
        });
        imgVuSandwich.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,OrderActivity.class);
                intent.putExtra("FoodName","Sandwich");
                intent.putExtra("Price","RS250");
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case (R.id.menu_setting):
                Toast.makeText(this, "Setting", Toast.LENGTH_SHORT).show();
                break;
            case (R.id.menu_about_us):
                Toast.makeText(this, "About us", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

   @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle("Exit");
        alertDialog.setMessage("Are you sure to exit ?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDialog.create();
        alertDialog.show();
    }
}
