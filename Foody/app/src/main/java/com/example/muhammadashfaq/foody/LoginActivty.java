package com.example.muhammadashfaq.foody;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.BoringLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class LoginActivty extends AppCompatActivity {
    Button btnSignup,btnLogin;
    ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectionDetector=new ConnectionDetector(this);
        setContentView(R.layout.activity_login);
        btnLogin=findViewById(R.id.btn_login);
        btnSignup=findViewById(R.id.btn_signup);
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivty.this,Signup.class);
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean network=connectionDetector.isConnected();
                if (network==true){
                    Snackbar.make(view,"Connected",Snackbar.LENGTH_LONG).show();
                }else {
                    Snackbar.make(view,"Can't connect right now",Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }
}
