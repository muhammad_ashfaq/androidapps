package com.example.muhammadashfaq.foody;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class OrderActivity extends AppCompatActivity {
    TextView txtVuLable,txtVuPrice;
    Button btnOrder,btnCancel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        Intent intent=getIntent();
        String foodname=intent.getStringExtra("FoodName");
        String foodprice=intent.getStringExtra("Price");
        setTitle(foodname);
        txtVuLable=findViewById(R.id.txt_vu_lable);
        txtVuPrice=findViewById(R.id.txt_vu_price);
        txtVuLable.setText(foodname);
        txtVuPrice.setText(foodprice);
        btnOrder=findViewById(R.id.btn_order);
        btnCancel=findViewById(R.id.btn_cancel);
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(OrderActivity.this,LoginActivty.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case (R.id.menu_setting):
                Toast.makeText(this, "Setting", Toast.LENGTH_SHORT).show();
                break;
            case (R.id.menu_about_us):
                Toast.makeText(this, "About us", Toast.LENGTH_SHORT).show();
        }
        return true;
    }


}
