package android.com.aalahazatkalam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        splashScreen();
    }
    private void splashScreen(){
        Thread thread=new Thread(){
            public void run(){
                super.run();
                try {
                    Thread.sleep(2000);
                    Intent mIntent=new Intent(SplashScreen.this,MainActivity.class);
                    startActivity(mIntent);
                    finish();
                }catch (Exception e){

                }
            }
        };
        thread.start();
    }
}
