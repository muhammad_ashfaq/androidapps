package example.android.com.admobproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends AppCompatActivity {
    AdView adView;
    Button btnFullscreenAd;
    Button btnBannerAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        adView = (AdView) findViewById(R.id.adview_banner_size);
        btnBannerAd=(Button)findViewById(R.id.btn_banner_ad);
        btnBannerAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdRequest adRequest = new AdRequest.Builder().build();
                adView.loadAd(adRequest);
            }
        });

        btnFullscreenAd = (Button) findViewById(R.id.btn_fullscreen_add);
        btnFullscreenAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,AddActivity.class));
            }
        });

    }

    @Override
     public void onPause() {
        if (adView != null){
            adView.pause();
        }
            super.onPause();
    }

    @Override
    public void onResume() {

        if(adView != null){
            adView.resume();
        }
        super.onResume();
    }
    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

}
