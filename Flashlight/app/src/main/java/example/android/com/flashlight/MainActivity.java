package example.android.com.flashlight;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Camera;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
    ImageButton imageBtnFlash;
    android.hardware.Camera cemra;
    android.hardware.Camera.Parameters parameters;
    boolean isFlash = false;
    boolean isOn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageBtnFlash = (ImageButton) findViewById(R.id.image_btn__flash);
        if (getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            cemra = android.hardware.Camera.open();
            parameters = cemra.getParameters();
            isFlash = true;

        }
        imageBtnFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFlash) {
                    if (!isOn) {
                        imageBtnFlash.setImageResource(R.drawable.on);
                        cemra.setParameters(parameters);
                        cemra.startPreview();
                        isOn= true;
                    } else {
                        imageBtnFlash.setImageResource(R.drawable.off);
                        parameters.setFlashMode(android.hardware.Camera.Parameters.FLASH_MODE_OFF);
                        cemra.setParameters(parameters);
                        cemra.stopPreview();
                        isOn = false;
                    }
                } else {
                    final AlertDialog.Builder alertdailoge = new AlertDialog.Builder(MainActivity.this);
                    alertdailoge.setTitle("Error");
                    alertdailoge.setMessage("Flash is not available on this device");
                    alertdailoge.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            finish();
                        }
                    });
                    AlertDialog alert = alertdailoge.create();
                    alert.show();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (cemra != null) {
            cemra.release();
            cemra = null;
        }
    }
}
