package example.android.com.toys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView textViewWeather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewWeather = (TextView) findViewById(R.id.txt_view_weather);
        String[] toyNames = ToyBox.getToyNames();
        for (String toyName : toyNames) {
            textViewWeather.append(toyName + "\n\n\n");

        }
    }
}
