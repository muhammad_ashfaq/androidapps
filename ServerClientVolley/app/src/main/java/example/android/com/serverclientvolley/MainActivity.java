package example.android.com.serverclientvolley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

public class MainActivity extends AppCompatActivity {
    Button btnOfServer;
    TextView txtVuOfServerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnOfServer=(Button)findViewById(R.id.btn_of_server);
        txtVuOfServerData=(TextView)findViewById(R.id.text_vu_server_data);


        btnOfServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest mStringRequest = new StringRequest(0, "http://192.168.1.123/Volley/jsonapi.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray myJsonResponse=new JSONArray(response);
                            String userName="";
                            String userEmail="";
                            String userPhnNum="";
                            String userAddress="";
                            for (int i=0;i<myJsonResponse.length();i++){
                                userName=myJsonResponse.getJSONObject(i).getString("user_name");
                                userEmail=myJsonResponse.getJSONObject(i).getString("user_email");
                                userPhnNum=myJsonResponse.getJSONObject(i).getString("user_phone");
                                userAddress=myJsonResponse.getJSONObject(i).getString("user_adress");

                                Toast.makeText(MainActivity.this,userName+ userEmail+  userPhnNum+ userAddress, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {

                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Server not connected", Toast.LENGTH_SHORT).show();
                    }
                });
                RequestQueue mRequestQueue=Volley.newRequestQueue(MainActivity.this);
                mRequestQueue.add(mStringRequest);

            }
        });

    }
}
