package android.com.recyclerviewimp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by AndroidBuddy on 2/15/2018.
 */

public class RecylerAdapter extends RecyclerView.Adapter<RecylerAdapter.RecylerViewHolder> {
    String[] array_username, array_location, array_time, array_likes, array_comments;
    int[] array_profile, array_picture;

    public RecylerAdapter(int[] array_profie, String[] array_username, String[] array_location, String[] array_time, int[] array_picture
            ,String[] array_likes, String[] array_comments) {
        this.array_profile = array_profie;
        this.array_username = array_username;
        this.array_location = array_location;
        this.array_time = array_time;
        this.array_picture = array_picture;
        this.array_likes = array_likes;
        this.array_comments = array_comments;
    }

    @Override
    public RecylerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_design,parent,false);
        RecylerViewHolder recylerViewHolder=new RecylerViewHolder(view);
        return recylerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecylerViewHolder holder, int position) {
        holder.txtVuUsername.setText(array_username[position]);
        holder.txtVuLocation.setText(array_location[position]);
        holder.txtVuTime.setText(array_time[position]);
        holder.txtVuLikes.setText(array_likes[position]);
        holder.txtVuComments.setText(array_comments[position]);
        holder.imgVuProfile.setImageResource(array_profile[position]);
        holder.imgVuPicture.setImageResource(array_picture[position]);
    }

    @Override
    public int getItemCount() {
        return array_username.length;
    }

    public static class RecylerViewHolder extends RecyclerView.ViewHolder {
        TextView txtVuUsername,txtVuLocation,txtVuTime,txtVuLikes,txtVuComments;
        ImageView imgVuProfile,imgVuPicture;
        public RecylerViewHolder(View view) {
            super(view);
            txtVuUsername=(TextView)view.findViewById(R.id.txt_vu_user_name);
            txtVuLocation=(TextView)view.findViewById(R.id.txt_vu_location);
            txtVuTime=(TextView)view.findViewById(R.id.txt_vu_time);
            txtVuLikes=(TextView)view.findViewById(R.id.txt_vu_likes);
            txtVuComments=(TextView)view.findViewById(R.id.txt_vu_comments);
            imgVuProfile=(ImageView)view.findViewById(R.id.img_vu_profile_pic);
            imgVuPicture=(ImageView)view.findViewById(R.id.img_vu_picture);
        }
    }
}