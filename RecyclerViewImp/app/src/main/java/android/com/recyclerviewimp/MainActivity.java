package android.com.recyclerviewimp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.Adapter recyclerAdapter;
    RecyclerView.LayoutManager layoutManager;
    int[] array_profile={
            R.drawable.profile_pic_1,R.drawable.profile_pic_2,R.drawable.profile_pic_3,R.drawable.profile_pic_4,
            R.drawable.profile_pic_5,R.drawable.profile_pic_6,R.drawable.profile_pic_7,R.drawable.profile_pic_8,R.drawable.profile_pic_9,
            R.drawable.profile_pic_10};
    int[] array_picture={R.drawable.picture_1,R.drawable.picture_2,R.drawable.picture_3,R.drawable.picture_4,
            R.drawable.picture_5,R.drawable.picture_6,R.drawable.picture_7,R.drawable.picture_8,
            R.drawable.picture_9,R.drawable.picture_10,};
    String[] arrry_username={"Muhammad Ashfaq","H M Riaz","H M Iftikhar","Syed Taimoor","Fahad Nisar","Farhan Afzal"
    ,"Zeeshan","Wasim Akram","Muhammad Arslan","Muhammad Rizwan"};
    String[] array_location={"Sargodha","Rawalpindi","Warcha","Faislabad","Texila","Lahore","Rawalpindi","Khushab","QaidaAbad","Muzafargarh"};
    String[] array_time={"2:19 pm","5:45 pm","6:09 pm","8:29 am","9:19 am","12:20 am","4:19 am","9:50 pm","10:59 pm","11:55 am"};
    String[] array_likes={"92Likes","99Likes","96Likes","76Likes","77Likes","67Likes","87Likes","67Likes","88Likes","98Likes"};
    String[] array_comments={"25Comments","45Comments","50Comments","34Comments","19Comments","56Comments","60Comments","56Comments","67Comments","70Comments"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView=(RecyclerView)findViewById(R.id.recyler_view);
        recyclerAdapter= new RecylerAdapter(array_profile,arrry_username,array_location,array_time, array_picture, array_likes, array_comments);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(recyclerAdapter);

    }
}
