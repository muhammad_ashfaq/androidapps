package android.com.listviewimp;

import android.com.listviewimp.Adapter.Apps_Adapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ListView lstVuApps;
    Apps_Adapter mApps_adapter;
    int[] array_thumbnails={
            R.drawable.first_pic,
            R.drawable.second_pic,
            R.drawable.third_pic,
            R.drawable.fourth_pic,
            R.drawable.five_pic,
            R.drawable.sixth_pic,
            R.drawable.seventh_pic,
            R.drawable.eighth,
            R.drawable.ninth_pic,
            R.drawable.tenth_pic
    };
    String[] array_devolpers={"Ashfaq","Mark","RizwanAtta","Maria","Madeeha","Aalia","BilalWarind","Ashfaq","Adeel","Shaziya"};
    String[] array_names={"Twitter","Facebook","Alibaba.com","daraz.pk","Amazon","CandyCrushSaga","TempleRun","Messneger","Whatsapp","Applock"};
    String[] array_rates={"4.7 *","4.6 *","4.4 *","4.5 *","4.7 *","4.6 *","4.4 *","4.5 *","4.7 *","4.6 *"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lstVuApps=(ListView)findViewById(R.id.lst_vu_apps_name);
        mApps_adapter=new Apps_Adapter(this,R.layout.lst_vu_design,array_thumbnails,array_names,array_devolpers,array_rates);
        lstVuApps.setAdapter(mApps_adapter);

        lstVuApps.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                
            }
        });
    }
    public void showMessage(){
        Toast.makeText(this, "List Implemented. Thumbs up", Toast.LENGTH_SHORT).show();
    }
}
