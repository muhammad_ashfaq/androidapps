package android.com.listviewimp.Adapter;

import android.com.listviewimp.R;
import android.content.Context;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by AndroidBuddy on 2/13/2018.
 */

public class Apps_Adapter extends ArrayAdapter {
    Context context;
    int resource;
    String[] app_names;
    String[] app_devolpers;
    String[] app_rates;
    int[] thumbnails;
    LayoutInflater mLayoutInflater;
    public Apps_Adapter(@NonNull Context context, int resource ,int[] thumbnails,String[] app_names,String[] app_devolpers,String[] app_rates) {
        super(context, resource);
        this.context=context;
        this.resource=resource;
        this.thumbnails=thumbnails;
        this.app_names=app_names;
        this.app_devolpers=app_devolpers;
        this.app_rates=app_rates;
        mLayoutInflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return thumbnails.length;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=mLayoutInflater.inflate(R.layout.lst_vu_design,null);
        ImageView img_vu_thumbnail=(ImageView)convertView.findViewById(R.id.img_vu_thumbnail);
        TextView txt_vu_app_names=(TextView)convertView.findViewById(R.id.txt_vu_app_name);
        TextView txt_vu_devolper_name=(TextView)convertView.findViewById(R.id.txt_vu_app_devlper_name);
        TextView txt_vu_app_rates=(TextView)convertView.findViewById(R.id.txt_vu_app_rate);

        img_vu_thumbnail.setImageResource(thumbnails[position]);
        txt_vu_app_names.setText(app_names[position]);
        txt_vu_devolper_name.setText(app_devolpers[position]);
        txt_vu_app_rates.setText(app_rates[position]);

        return convertView;
    }
}
