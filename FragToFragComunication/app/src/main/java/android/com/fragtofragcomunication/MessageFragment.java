package android.com.fragtofragcomunication;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment {
    Button button;
    EditText editText;
    OnMessageSendListner onMessageSendListner;

    public interface OnMessageSendListner {
        public void onReadMessage(String message);

    }

    public MessageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        button = (Button) view.findViewById(R.id.btn_send);
        editText = (EditText) view.findViewById(R.id.edt_txt);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String message=editText.getText().toString();
                onMessageSendListner.onReadMessage(message);

            }
        }
        );
        return view;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;
        try {
            onMessageSendListner = (OnMessageSendListner) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must implement OnReadMessage()");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        editText.setText("");
    }
}
