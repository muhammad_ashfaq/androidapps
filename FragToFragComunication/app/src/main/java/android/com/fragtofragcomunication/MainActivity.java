
package android.com.fragtofragcomunication;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements MessageFragment.OnMessageSendListner {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(findViewById(R.id.fragment_container)!=null){
            if(savedInstanceState!=null){
                return;
            }
            MessageFragment messageFragment=new MessageFragment();
            android.support.v4.app.FragmentManager fragmentManager=getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.fragment_container,messageFragment,null);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onReadMessage(String message) {
        DisplayFragment displayFragment=new DisplayFragment();
        Bundle bundle=new Bundle();
        bundle.putString("message",message);
        displayFragment.setArguments(bundle);
       android.support.v4.app.FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,displayFragment,null);
       fragmentTransaction.addToBackStack(null);
       fragmentTransaction.commit();


    }
}
