package example.android.com.fragmets;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

import example.android.com.fragmets.Fragments.FirstFragment;
import example.android.com.fragmets.Fragments.SecondFragment;

public class MainActivity extends AppCompatActivity {
    RelativeLayout relativeLayoutUp;
    RelativeLayout relativeLayoutDown;
    FirstFragment firstFragment;
    SecondFragment secondFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        relativeLayoutUp=(RelativeLayout) findViewById(R.id.rel_lay_up);
        relativeLayoutDown=(RelativeLayout) findViewById(R.id.rel_lay_down);
        firstFragment=new FirstFragment();
        secondFragment=new SecondFragment();
        //Fragments main parts attaching with an activity.
        /*First fragment work here.*/
        FragmentManager firstFragmentManager=getSupportFragmentManager();
        FragmentTransaction firstFragmentTransaction=firstFragmentManager.beginTransaction();
        firstFragmentTransaction.add(R.id.rel_lay_up,firstFragment);
        firstFragmentTransaction.commit();

        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction secondFragmentTransaction=fragmentManager.beginTransaction();
        secondFragmentTransaction.add(R.id.rel_lay_down,secondFragment);
        secondFragmentTransaction.commit();

    }
}
