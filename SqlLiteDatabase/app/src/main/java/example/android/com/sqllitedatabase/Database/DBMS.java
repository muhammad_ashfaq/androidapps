package example.android.com.sqllitedatabase.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by JackReacher on 9/23/2017.
 */

public class DBMS extends SQLiteOpenHelper {
    /*-----"final"==>>our app db name can't be changed by anyone ie hacker or other person.*/
    /*-----"static==>>becoz to available it for all classes in our java code.to make it our visible for everyone.*/
    /*-----"myUserDatabase.db"==>> with .db extention our db is immutable like string which can't be changed or altered.*/
    /*-----"myUserDatabase.sqllite"==>> with .sqllite extention our db is mutable mean can be altered or changed.*/

    public static final String dbName = "myUserDatabase.db";
    public static int dbVersion = 1;
    public String createTableQuery = "CREATE TABLE userDetails(user_name TEXT,user_email TEXT,user_password TEXT,user_phn_number INT,user_adress TEXT);";

    /*----"null"==>> null is here cursor factory. we will write our own factory later then it value will not be null*/
    public DBMS(Context context) {
        super(context, dbName, null, dbVersion);
        /*Constructer will create db for us if it doesn't exist already.*/
        Log.e("cotext", "db created.well done !!!");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    /* onCreate is one who creates structure of table.It always work once when app is installed.*/
        db.execSQL(createTableQuery);
        Log.e("onCreate", "table created.well done !!!");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void getInsertUser(String incomingUsrName, String incomingUsrEmail, String incomingUsrPassword, String incomingUsrPhnNumber, String incomnigUsrAdress, SQLiteDatabase db) {
        ContentValues cv = new ContentValues();
        cv.put("user_name", incomingUsrName);
        cv.put("user_email", incomingUsrEmail);
        cv.put("user_password", incomingUsrPassword);
        cv.put("user_phn_number", incomingUsrPhnNumber);
        cv.put("user_adress", incomnigUsrAdress);

        db.insert("userDetails", null, cv);
    }

    public Cursor getAllRegisterdUsers(SQLiteDatabase db) {
        //Projection
        String[] myColProjection = {"user_name", "user_email", "user_password", "user_phn_number", "user_adress"};
        Cursor myCursor = db.query("userDetails", myColProjection, null, null, null, null, null);
        return myCursor;
    }

    public boolean checkUserLogin(String incomingLoginUsrEmail, String incomingLoginUsrPassword, SQLiteDatabase db) {
        String[] selectionArgs = {incomingLoginUsrEmail, incomingLoginUsrPassword};
         Cursor myResultCursor=db.rawQuery("SELECT user_email,user_password FROM userDetails WHERE user_email='"+incomingLoginUsrEmail+"' and user_password='"+incomingLoginUsrPassword+"'",null);
        if (myResultCursor.getCount()==0) {
            return false;
        }else{
            return true;
        }

    }
}