package example.android.com.sqllitedatabase;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.math.MathUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import example.android.com.sqllitedatabase.Database.DBMS;

public class LoginActivity extends AppCompatActivity {
    EditText edtTxtLoginUsrEmail,edtTxtLoginUsrPassword;
    String incomingLoginUsrEmail,incomingLoginUsrPassword;
    Button btnLoginUsr;
    DBMS dbms;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        edtTxtLoginUsrEmail=(EditText)findViewById(R.id.edt_txt_login_user_email);
        edtTxtLoginUsrPassword=(EditText)findViewById(R.id.edt_txt_login_user_password);
        btnLoginUsr=(Button) findViewById(R.id.btn_login_user);

        dbms=new DBMS(LoginActivity.this);
        btnLoginUsr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                incomingLoginUsrEmail = edtTxtLoginUsrEmail.getText().toString();
                incomingLoginUsrPassword = edtTxtLoginUsrPassword.getText().toString();
                if (incomingLoginUsrEmail.isEmpty() || incomingLoginUsrPassword.isEmpty()) {
                    if (incomingLoginUsrEmail.isEmpty()) {
                        edtTxtLoginUsrEmail.setError("Please Enter Email");
                    } else if (incomingLoginUsrPassword.isEmpty()) {
                        edtTxtLoginUsrPassword.setError("Enter Password");

                    }
                } else {
                    db = dbms.getReadableDatabase();
                    boolean incomingLoginResponse = dbms.checkUserLogin(incomingLoginUsrEmail, incomingLoginUsrPassword, db);
                    if (incomingLoginResponse == true) {

                    } else if(incomingLoginResponse==false) {
                        Toast.makeText(LoginActivity.this, "User not Registered.Please Sign up", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    public void goToPage(View v){
        Intent myIntent=new Intent(LoginActivity.this, MainActivity.class);
        startActivity(myIntent);

    }
}
