package example.android.com.sqllitedatabase;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import example.android.com.sqllitedatabase.Database.DBMS;

public class MainActivity extends AppCompatActivity {
    EditText edtTxtRegUsrName, edtTxtRegUsrEmail, edtTxtRegUsrPassword, edtTxtRegUsrPhnNumber, edtTxtRegUsrAdress;
    String incomingUsrName, incomingUsrEmail, incomingUsrPassword, incomingUsrPhnNumber, incomnigUsrAdress;
    Button btnRegUsr, btnGetAllRegUsers;
    DBMS dbms;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtTxtRegUsrName = (EditText) findViewById(R.id.edt_txt_reg_user_name);
        edtTxtRegUsrEmail = (EditText) findViewById(R.id.edt_txt_reg_user_email);
        edtTxtRegUsrPassword = (EditText) findViewById(R.id.edt_txt_reg_user_password);
        edtTxtRegUsrPhnNumber = (EditText) findViewById(R.id.edt_txt_reg_user_phn_number);
        edtTxtRegUsrAdress = (EditText) findViewById(R.id.edt_txt_reg_user_adress);
        btnRegUsr = (Button) findViewById(R.id.btn_reg_user);
        btnGetAllRegUsers = (Button) findViewById(R.id.btn_get_all_reg_user);

        dbms = new DBMS(MainActivity.this);

        btnRegUsr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                incomingUsrName = edtTxtRegUsrName.getText().toString();
                incomingUsrEmail = edtTxtRegUsrEmail.getText().toString();
                incomingUsrPassword = edtTxtRegUsrPassword.getText().toString();
                incomingUsrPhnNumber = edtTxtRegUsrPhnNumber.getText().toString();
                incomnigUsrAdress = edtTxtRegUsrAdress.getText().toString();
                if (incomingUsrName.isEmpty()
                        || incomingUsrEmail.isEmpty()
                        || incomingUsrPassword.isEmpty()
                        || incomingUsrPhnNumber.isEmpty()
                        || incomnigUsrAdress.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Please Fill the Form.", Toast.LENGTH_SHORT).show();
                    edtTxtRegUsrName.setError("Please Enter User Name");
                    edtTxtRegUsrEmail.setError("Please Enter Email");
                    edtTxtRegUsrPassword.setError("Please Enter Password");
                    edtTxtRegUsrPhnNumber.setError("Please Enter User Name");
                    edtTxtRegUsrAdress.setError("Please Enter Adress");
                } else {
                    db = dbms.getWritableDatabase();
                    dbms.getInsertUser(incomingUsrName, incomingUsrEmail, incomingUsrPassword, incomingUsrPhnNumber, incomnigUsrAdress, db);
                    Toast.makeText(MainActivity.this, "User Registered.", Toast.LENGTH_SHORT).show();
                    edtTxtRegUsrName.setText("");
                    edtTxtRegUsrEmail.setText("");
                    edtTxtRegUsrPassword.setText("");
                    edtTxtRegUsrPhnNumber.setText("");
                    edtTxtRegUsrAdress.setText("");
                }
            }
        });
        btnGetAllRegUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db = dbms.getReadableDatabase();
                /*this will give filled cursor with all values.*/
                Cursor incomingDbCursor = dbms.getAllRegisterdUsers(db);
                if (incomingDbCursor.moveToFirst()) {
                    do {
                        String username=incomingDbCursor.getString(0);
                        String useremail=incomingDbCursor.getString(1);
                        String userpassword=incomingDbCursor.getString(2);
                        String userphnnumber=incomingDbCursor.getString(3);
                        String useradress=incomingDbCursor.getString(4);
                        Toast.makeText(MainActivity.this,username+" "+useremail+" "+userpassword+" "+userphnnumber+" "+useradress, Toast.LENGTH_SHORT).show();
                    } while (incomingDbCursor.moveToNext());
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
