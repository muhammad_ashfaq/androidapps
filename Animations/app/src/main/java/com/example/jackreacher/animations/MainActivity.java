package com.example.jackreacher.animations;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btnAlpha;
    Button btnScale;
    Button btnTranslate;
    Button btnRotate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnAlpha=(Button)findViewById(R.id.btn_alpha);
        btnScale=(Button)findViewById(R.id.btn_scale);
        btnTranslate=(Button)findViewById(R.id.btn_translate);
        btnRotate=(Button)findViewById(R.id.btn_rotate);

        btnAlpha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlphaAnimation alphaAnimation=new AlphaAnimation(1.0f,0.0f);
                alphaAnimation.setDuration(1000);
                btnAlpha.startAnimation(alphaAnimation);
            }
        });
        btnScale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScaleAnimation scaleAnimation=new ScaleAnimation(1.0f,3.0f,1.0f,3.0f);
                scaleAnimation.setDuration(1000);
                btnScale.startAnimation(scaleAnimation);
            }
        });
        btnRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RotateAnimation rotateAnimation=new RotateAnimation(1.0f,3.0f,1.0f,3.0f);
                rotateAnimation.setDuration(1000);
                btnRotate.startAnimation(rotateAnimation);
            }
        });
        btnTranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScaleAnimation scaleAnimation=new ScaleAnimation(1.0f,3.0f,1.0f,3.0f);
                scaleAnimation.setDuration(1000);
                btnTranslate.startAnimation(scaleAnimation);
            }
        });


    }
}
