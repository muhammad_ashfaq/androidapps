package com.example.muhammadashfaq.chatroom;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class StartActivty extends AppCompatActivity {
    Button btnStartReg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_activty);
        btnStartReg=findViewById(R.id.btn_start_reg);
        btnStartReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regIntent=new Intent(StartActivty.this,RegisterActivity.class);
                startActivity(regIntent);
            }
        });
    }
}
