package com.example.muhammadashfaq.chatroom;

import android.annotation.TargetApi;
import android.content.Intent;
import android.drm.DrmStore;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import android.R.*;

public class RegisterActivity extends AppCompatActivity {
    TextInputEditText edtTextName, edtTextEmail, edtTextPassword;
    Button btnRegister;
    //Firebase Auth
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Registration fields
        setContentView(R.layout.activity_register);
        edtTextName = findViewById(R.id.edt_txt_name);
        edtTextEmail=findViewById(R.id.edt_txt_email);
        edtTextPassword = findViewById(R.id.edt_txt_password);
        btnRegister = findViewById(R.id.btn_register);

        //Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        //Register button listner
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String incomingName = edtTextName.getText().toString();
                String incomingEmail = edtTextEmail.getText().toString();
                String incomingPassword = edtTextPassword.getText().toString();

                registrUser(incomingName, incomingEmail, incomingPassword);

            }
        });
    }

    //Firebase register new user method
    private void registrUser(final String incomingName, final String incomingEmail, final String incomingPassword)
    {
        mAuth.createUserWithEmailAndPassword(incomingEmail, incomingPassword)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
                    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        boolean validation = stdFormValidate(incomingName, incomingEmail, incomingPassword);
                        if (validation == true) {
                            if (task.isSuccessful()) {
                                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                startActivity(intent);
                            } else {
                                FirebaseAuthException e = (FirebaseAuthException) task.getException();
                                Toast.makeText(RegisterActivity.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(RegisterActivity.this, "Please Enter Valid Data", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    public boolean stdFormValidate(String name, String email, String password){
        boolean valid=true;
        if(name.isEmpty() || name.length()>32)
        {
            edtTextName.setError("Please enter a valid Name");
            valid=false;
        }
        if(password.isEmpty()){
            edtTextPassword.setError("Please enter Password");
            valid=false;
        }
        if (password.length()<6){
            edtTextPassword.setError("Please enter 6-digit passwoord");
            valid=false;
        }
        return valid;
    }
}