package example.android.com.hashmapserverapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class SignUpAcitivty extends AppCompatActivity {
    EditText edtTxtName,edtTxtEmail,edtTxtPassword,edtTxtAddress,edtTxtPhnNumber;
    String incomingName,incomingEmail, incomingPassword,incomingAddress,incomingPhnNumber;
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtTxtName=(EditText)findViewById(R.id.edt_txt_name);
        edtTxtEmail=(EditText)findViewById(R.id.edt_txt_email);
        edtTxtPassword=(EditText)findViewById(R.id.edt_txt_password);
        edtTxtAddress=(EditText)findViewById(R.id.edt_txt_address);
        edtTxtPhnNumber=(EditText)findViewById(R.id.edt_txt_phn_number);
        btnRegister=(Button)findViewById(R.id.btn_get_started);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incomingName=edtTxtName.getText().toString();
                incomingEmail=edtTxtEmail.getText().toString();
                incomingPassword=edtTxtPassword.getText().toString();
                incomingAddress=edtTxtAddress.getText().toString();
                incomingPhnNumber=edtTxtPhnNumber.getText().toString();

                StringRequest mStringRequest=new StringRequest(1,"http://192.168.10.2/Volley/runtimevalues.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(SignUpAcitivty.this,response, Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SignUpAcitivty.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> myValuesMap=new HashMap<String, String>();
                        myValuesMap.put("userName",incomingName);
                        myValuesMap.put("userEmail",incomingEmail);
                        myValuesMap.put("userPassword",incomingPassword);
                        myValuesMap.put("userAddress",incomingAddress);
                        myValuesMap.put("userPhnNumber",incomingPhnNumber);
                        return myValuesMap;
                    }
                };
                RequestQueue mRequestQueue= Volley.newRequestQueue(SignUpAcitivty.this);
                mRequestQueue.add(mStringRequest);
            }
        });

    }
}
