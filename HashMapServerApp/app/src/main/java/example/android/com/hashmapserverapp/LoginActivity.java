package example.android.com.hashmapserverapp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;
import java.util.zip.Inflater;

import example.android.com.hashmapserverapp.LoginSessionManager.SessionManagerClass;

/**
 * Created by JackReacher on 10/11/2017.
 */

public class LoginActivity extends AppCompatActivity{
    EditText edtTxtLoginEmail;
    EditText edtTxtLoginPassword;
    String incomingLoginEmail;
    String incomingLoginPassword;
    Button btnLogin,btnSignup;
    SessionManagerClass sessionManagerClass;
    CheckInternetConnection checkInternetConnection=new CheckInternetConnection();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
      /*  if(checkInternetConnection.isInternetConnection()==false){
            LayoutInflater mLayoutInflater=getLayoutInflater();
            View layout= mLayoutInflater.inflate(R.layout.custom_toast_layout,(ViewGroup)findViewById(R.id.custom_toast_container));
            TextView txtVuToast=(TextView)layout.findViewById(R.id.txt_vu_toast);
            txtVuToast.setText("Hmm... you're not connected to Internet");
            Toast mtoast=new Toast(getApplicationContext());
            mtoast.setDuration(Toast.LENGTH_LONG);
            mtoast.setView(layout);
            mtoast.show();
        }*/
//        Always make object here.
        sessionManagerClass=new SessionManagerClass(LoginActivity.this);
        if (sessionManagerClass.checkUserLogin()==true){
            Intent i=new Intent(LoginActivity.this,Homepage.class);
            startActivity(i);
            finish();
        }
        edtTxtLoginEmail=(EditText)findViewById(R.id.edt_txt_login_email);
        edtTxtLoginPassword=(EditText)findViewById(R.id.edt_txt_login_password);
        btnLogin=(Button)findViewById(R.id.btn_login);
        btnSignup=(Button)findViewById(R.id.btn_signup);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incomingLoginEmail=edtTxtLoginEmail.getText().toString();
                incomingLoginPassword=edtTxtLoginPassword.getText().toString();
                if(incomingLoginEmail.isEmpty() || incomingLoginPassword.isEmpty()){
                    if(incomingLoginEmail.isEmpty()){
                        edtTxtLoginEmail.setError("Please Enter Email");
                    }else if(incomingLoginPassword.isEmpty()){
                        edtTxtLoginPassword.setError("Please Enter Password");
                    }
                }

                StringRequest mStringRequest=new StringRequest(1, "http://192.168.0.109/Volley/loginscript.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equalsIgnoreCase("true")){
                            sessionManagerClass.loginUserSession(true,incomingLoginEmail,incomingLoginPassword);
                            Intent mIntent=new Intent(LoginActivity.this,Homepage.class);
                            startActivity(mIntent);
                            finish();
                            LayoutInflater mLayoutInflater=getLayoutInflater();
                            View layout= mLayoutInflater.inflate(R.layout.custom_toast_layout,(ViewGroup)findViewById(R.id.custom_toast_container));
                            TextView txtVuToast=(TextView)layout.findViewById(R.id.txt_vu_toast);
                            txtVuToast.setText("Hmm... Congratulations");
                            Toast mtoast=new Toast(getApplicationContext());
                            mtoast.setDuration(Toast.LENGTH_LONG);
                            mtoast.setView(layout);
                            mtoast.show();
                        }
                        else{
                            Toast.makeText(LoginActivity.this, "false", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> mValuesParem=new HashMap<String, String>();
                        mValuesParem.put("userLoginEmail",incomingLoginEmail);
                        mValuesParem.put("userLoginPassword",incomingLoginPassword);
                        return mValuesParem;
                    }
                };
                RequestQueue mRequestQueue= Volley.newRequestQueue(LoginActivity.this);
                mRequestQueue.add(mStringRequest);
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent=new Intent(LoginActivity.this,SignUpAcitivty.class);
                startActivity(mIntent);
            }
        });
    }
}

