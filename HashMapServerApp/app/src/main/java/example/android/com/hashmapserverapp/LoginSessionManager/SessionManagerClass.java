package example.android.com.hashmapserverapp.LoginSessionManager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by JackReacher on 10/13/2017.
 */

public class SessionManagerClass {
    SharedPreferences mPref;
    SharedPreferences.Editor mEditor;
    String prefName="LoginPref";
    boolean logginIn=false;
    /*Every pref has its own type.*/
    /*Like public ,private ,specifly protected.*/
    /*0 ==> stands for private : 1 ==> stands for public : 2 ==> stands for specifecly protected*/
    public SessionManagerClass(Context context){
        mPref=context.getSharedPreferences(prefName,0);
        mEditor=mPref.edit();
    }
    public void loginUserSession(boolean loggedIn,String email,String password){
        mEditor.putBoolean("userLoginType",loggedIn);
        mEditor.putString("userEmail",email);
        mEditor.putString("userPassword",password);
        mEditor.commit();
    }
    /*A Test method weather user is already logged in or not.*/
    public boolean checkUserLogin(){
        return mPref.getBoolean("userLoginType",false);
    }
}
