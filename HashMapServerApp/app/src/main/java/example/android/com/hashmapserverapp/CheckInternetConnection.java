package example.android.com.hashmapserverapp;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by JackReacher on 10/15/2017.
 */

public class CheckInternetConnection extends Activity {
    public boolean isInternetConnection(){
        Context context=new LoginActivity();
        boolean conneted;
        ConnectivityManager connectivityManager=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState()== NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState()==NetworkInfo.State.CONNECTED){
            conneted=true;
        }else{
            conneted=false;
        }

        return conneted;
    }
}
