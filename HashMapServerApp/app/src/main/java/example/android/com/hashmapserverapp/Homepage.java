package example.android.com.hashmapserverapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import example.android.com.hashmapserverapp.LoginSessionManager.SessionManagerClass;

public class Homepage extends AppCompatActivity {
    SessionManagerClass sessionManagerClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        sessionManagerClass=new SessionManagerClass(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optn_menu_logout,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.opt_menu_logout){
            sessionManagerClass.loginUserSession(false,"","");
            Intent mIntent=new Intent(Homepage.this,LoginActivity.class);
            startActivity(mIntent);
            finish();
        }
        return true;
    }
}
