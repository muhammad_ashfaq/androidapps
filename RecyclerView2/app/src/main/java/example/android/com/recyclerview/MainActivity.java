package example.android.com.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
             /* -------Linking our Adapter to recycler view.--------*/
        recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        MyAdapter myAdapter=new MyAdapter(MainActivity.this, Data.getObjectList());
        recyclerView.setAdapter(myAdapter);

        /*----Linking LayoutManager to recycler view*/
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(MainActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager) ;
    }
}
