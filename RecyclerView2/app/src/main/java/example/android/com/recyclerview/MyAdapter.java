package example.android.com.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by JackReacher on 10/10/2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

    private List<Data> objectList;
    private LayoutInflater layoutInflater;


    public MyAdapter(Context context,List<Data> dataList){
        layoutInflater= LayoutInflater.from(context);
        this.objectList= dataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= layoutInflater.inflate(R.layout.list_item,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
    Data current=objectList.get(position);
        holder.setData(current,position);
        holder.setLisnters();
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title;
        private ImageView imageThumb,imageCopy,imageDelete;
        private int position;
        Data currentObject;

        public MyViewHolder(View itemView) {
            super(itemView);
            title=(TextView)itemView.findViewById(R.id.txt_vu_title1);
            imageThumb=(ImageView)itemView.findViewById(R.id.image_vu_pic);
            imageCopy=(ImageView)itemView.findViewById(R.id.image_vu_copy);
            imageDelete=(ImageView)itemView.findViewById(R.id.image_vu_delete);
        }

        public void setData(Data currentObject, int position) {
            this.title.setText(currentObject.getTitle());
            this.imageThumb.setImageResource(currentObject.getImageId());
            this.position=position;
            this.currentObject=currentObject;
        }

        public void setLisnters() {
            imageDelete.setOnClickListener(MyViewHolder.this);
            imageCopy.setOnClickListener( MyViewHolder.this);
            imageThumb.setOnClickListener( MyViewHolder.this);
        }
        public void onClick(View v){
            switch (v.getId()){
                case R.id.image_vu_delete:
                    removeItem(position);
                    break;
                case R.id.image_vu_copy:
                    addItem(position,currentObject);
                    break;
            }
        }
        public void removeItem(int position){
            objectList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position,objectList.size());
        }
        public void addItem(int position,Data currentObject){
            objectList.add(position,currentObject);
            notifyItemInserted(position);
            notifyItemRangeChanged(position,objectList.size());
           notifyDataSetChanged();
        }
    }


}
