package android.com.runtimefragments;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ToolbarWidgetWrapper;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    public static android.support.v4.app.FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        fragmentManager=getSupportFragmentManager();
        /*First condition checks weather container is defined or not for fragments*/
        if(findViewById(R.id.fragments_container)!=null){
            /*this condition is specified to check that the activity must not be in resumed state.
            * if this condition is not applied fragments overlapping will take place.*/
            if(savedInstanceState!=null){
                return;
            }
            android.support.v4.app.FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
            HomeFragment homeFragment=new HomeFragment();
            fragmentTransaction.add(R.id.fragments_container,homeFragment,null);
            fragmentTransaction.commit();


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.homeAsUp){
        finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
