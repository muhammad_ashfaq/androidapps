package example.android.com.intents;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import example.android.com.intents.Adapter.ViewPagerAdapter;
import example.android.com.intents.TeacherFragments.TeacherProfileFragment;
import example.android.com.intents.TeacherFragments.TeacherQuizFragment;
import example.android.com.intents.TeacherFragments.TeacherResultFragment;

public class TeacherHomeActivity extends AppCompatActivity {
    Toolbar toolbar_teacher;
    TabLayout tabLayoutTeacher;
    ViewPager viewPagerTeacher;
    ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_home);
        toolbar_teacher = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar_teacher);
        viewPagerTeacher = (ViewPager) findViewById(R.id.view_pager_teacher);
        tabLayoutTeacher = (TabLayout) findViewById(R.id.tab_layout_teacher);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new TeacherProfileFragment(), "Profile");
        viewPagerAdapter.addFragments(new TeacherQuizFragment(), "Quiz");
        viewPagerAdapter.addFragments(new TeacherResultFragment(), "Result");
        viewPagerTeacher.setAdapter(viewPagerAdapter);
        tabLayoutTeacher.setupWithViewPager(viewPagerTeacher);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
*/
    }
}