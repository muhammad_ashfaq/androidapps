package example.android.com.intents;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import example.android.com.intents.dbmspkg.TeacherDBMS;

public class SignupTeacher extends AppCompatActivity {
    TextInputEditText editTextTchrFullName,editTextTchrEmail,editTextTchrPassword,editTextTchrPhnNumber;
    String incomingTchrFullName,incomingTchrEmail, incomingTchrPassword,incomingTchrPhnNumber;
    TextView txtVuGetStarted;
    TeacherDBMS teacherDBMS;
    SQLiteDatabase sqldb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_teacher);
        editTextTchrFullName=(TextInputEditText) findViewById(R.id.edt_txt_teacher_fullname);
        editTextTchrEmail=(TextInputEditText) findViewById(R.id.edt_txt_teacher_email);
        editTextTchrPassword=(TextInputEditText) findViewById(R.id.edt_txt_teacher_password);
        editTextTchrPhnNumber=(TextInputEditText) findViewById(R.id.edt_txt_teacher_phn_number);

        txtVuGetStarted=(TextView) findViewById(R.id.txt_vu_teacher_get_started);
        teacherDBMS=new TeacherDBMS(SignupTeacher.this);
        txtVuGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                incomingTchrFullName =editTextTchrFullName.getText().toString();
                incomingTchrEmail=editTextTchrEmail.getText().toString();
                incomingTchrPassword =editTextTchrPassword.getText().toString();
                incomingTchrPhnNumber=editTextTchrPhnNumber.getText().toString();
                if(tchrFormValidate()){
                    sqldb=teacherDBMS.getWritableDatabase();
                    teacherDBMS.insertNewTeacher(incomingTchrFullName,incomingTchrEmail,incomingTchrPassword,incomingTchrPhnNumber,
                            sqldb);
                    Intent mIntent=new Intent(SignupTeacher.this,TeacherHomeActivity.class);
                    startActivity(mIntent);
                    Toast.makeText(SignupTeacher.this, "Congrates", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }
    public boolean tchrFormValidate(){
        boolean valid=true;
        if(incomingTchrFullName.isEmpty() || incomingTchrFullName.length()>32){
            editTextTchrFullName.setError("Please enter a valid Name");
            valid=false;
        }
        if(incomingTchrEmail.isEmpty() ||  !Patterns.EMAIL_ADDRESS.matcher(incomingTchrEmail).matches()){
            editTextTchrEmail.setError("Please enter a valid Email");
            valid=false;
        }
        if(incomingTchrPassword.isEmpty()){
            editTextTchrPassword.setError("Please enter Password");
            valid=false;
        }
        if(incomingTchrPhnNumber.isEmpty()){
            editTextTchrPhnNumber.setError("Please enter Phone number");
            valid=false;
        }
        return valid;
    }
}
