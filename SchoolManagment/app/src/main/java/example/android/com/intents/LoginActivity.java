package example.android.com.intents;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import example.android.com.intents.dbmspkg.StudentDBMS;
import example.android.com.intents.dbmspkg.TeacherDBMS;

public class LoginActivity extends AppCompatActivity {
    EditText edtTextEmail, edtTextPassword;
    String incomingEmail, incomingPassword;
    TextView txtVuCreateNewAccount;
    TextView txtVuContinue;
    StudentDBMS studentDBMS;
    TeacherDBMS teacherDBMS;
    SQLiteDatabase sqLiteDatabase;
    SQLiteDatabase database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        edtTextEmail=(EditText) findViewById(R.id.edt_txt_login_email);
        edtTextPassword=(EditText) findViewById(R.id.edt_txt_login_password);
        txtVuContinue=(TextView) findViewById(R.id.txt_vu_continue);
        studentDBMS=new StudentDBMS(LoginActivity.this);
        teacherDBMS=new TeacherDBMS(LoginActivity.this);
        txtVuContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                incomingEmail=edtTextEmail.getText().toString();
                incomingPassword=edtTextPassword.getText().toString();
                if(incomingEmail.isEmpty() || incomingPassword.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Enter email and password", Toast.LENGTH_SHORT).show();
                }else{
                    sqLiteDatabase=studentDBMS.getReadableDatabase();
                    database=teacherDBMS.getReadableDatabase();
                    boolean incomingResultCursor=studentDBMS.checkStudentLogin(incomingEmail,incomingPassword,sqLiteDatabase);
                    boolean incomingCursor=teacherDBMS.checkTeacherLogin(incomingEmail,incomingPassword,database);
                    if(incomingCursor){
                        Intent mIntent = new Intent(LoginActivity.this, TeacherHomeActivity.class);
                        startActivity(mIntent);
                        finish();
                    }else if(incomingResultCursor){
                        Intent mIntent = new Intent(LoginActivity.this, StudentHomeActivity.class);
                        startActivity(mIntent);
                        finish();
                    }
                }
            }
        });
        txtVuCreateNewAccount = (TextView) findViewById(R.id.txt_vu_create_new_account);
        txtVuCreateNewAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(mIntent);
            }
        });
    }
}