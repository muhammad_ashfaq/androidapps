package example.android.com.intents.dbmspkg;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by AndroidBuddy on 11/13/2017.
 */

public class TeacherDBMS extends SQLiteOpenHelper {
    public static final String dbName="TeacherDBMS.db";
    public static int dbVersion=1;
    public boolean dbTeacherStatus;
    public String createTeacherTableQuery="CREATE TABLE teacherDetails(tchr_name TEXT,tchr_email TEXT,tchr_password TEXT,tchr_phn_nmbr TEXT);";
    public TeacherDBMS(Context context){
        super(context,dbName,null,dbVersion);
        Log.e("Teacher DB Constructer","Good....DB instance created.");
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createTeacherTableQuery);
        Log.i("onCreate===>>>","structure created.well done buddy");
        dbTeacherStatus=true;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void insertNewTeacher(String incomingTchrFullName, String incomingTchrEmail, String incomingTchrPassword,
                                 String incomingTchrPhnNumber, SQLiteDatabase sqldb) {
        ContentValues contentValues=new ContentValues();
        contentValues.put("tchr_name",incomingTchrFullName);
        contentValues.put("tchr_email",incomingTchrEmail);
        contentValues.put("tchr_password",incomingTchrPassword);
        contentValues.put("tchr_phn_nmbr",incomingTchrPhnNumber);
        sqldb.insert("teacherDetails",null,contentValues);
    }

    public boolean checkTeacherLogin(String incomingEmail, String incomingPassword, SQLiteDatabase database) {
        Cursor resultCursor;
        resultCursor=database.rawQuery("select tchr_email and tchr_password from teacherDetails where tchr_email='"+incomingEmail+"' and tchr_password='"+incomingPassword+"'",null);
        if (resultCursor.getCount()==1){
            return true;
        }else{
            return false;
        }
    }
}