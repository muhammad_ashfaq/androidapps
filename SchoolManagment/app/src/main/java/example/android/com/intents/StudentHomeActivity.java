package example.android.com.intents;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import example.android.com.intents.Adapter.ViewPagerStdAdapter;
import example.android.com.intents.StudentFragments.StdAnnouncmentsFragment;
import example.android.com.intents.StudentFragments.StdCheckResultFragment;
import example.android.com.intents.StudentFragments.StdInformationFragment;

public class StudentHomeActivity extends AppCompatActivity{
    Toolbar toolbar;
    TabLayout tabLayoutStd;
    ViewPager viewPagerStd;
    ViewPagerStdAdapter viewPagerStdAdapter;
    String stdName,stdEmail,stdPassword,stdClass,stdRollNo;
    StdInformationFragment stdInformationFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_home);
        stdInformationFragment=new StdInformationFragment();
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tabLayoutStd=(TabLayout)findViewById(R.id.tab_layout_student);
        viewPagerStd=(ViewPager)findViewById(R.id.view_pager_student);
        viewPagerStdAdapter=new ViewPagerStdAdapter(getSupportFragmentManager());
        viewPagerStdAdapter.addFragments(new StdInformationFragment(),"Profile");
        viewPagerStdAdapter.addFragments(new StdCheckResultFragment(),"Result");
        viewPagerStdAdapter.addFragments(new StdAnnouncmentsFragment(),"Teachers");
        viewPagerStd.setAdapter(viewPagerStdAdapter);
        tabLayoutStd.setupWithViewPager(viewPagerStd);

        Intent callerIntent=getIntent();
        stdName=callerIntent.getStringExtra("stdname");
        stdEmail=callerIntent.getStringExtra("stdemail");
        stdPassword=callerIntent.getStringExtra("stdpassword");
        stdClass=callerIntent.getStringExtra("stdclass");
        stdRollNo=callerIntent.getStringExtra("stdrollno");
    }
}
