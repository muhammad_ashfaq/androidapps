package example.android.com.intents.StudentFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import example.android.com.intents.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class StdCheckResultFragment extends Fragment {


    public StdCheckResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_std_check_result, container, false);
    }

}
