package example.android.com.intents.StudentFragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.R.*;
import android.widget.Toast;


import example.android.com.intents.R;
import example.android.com.intents.StudentHomeActivity;
import example.android.com.intents.dbmspkg.StudentDBMS;

/**
 * A simple {@link Fragment} subclass.
 */
public class StdInformationFragment extends Fragment {
    public TextView txtVuStdName,txtVuStdEmail,txtVuStdPassword,txtVuStdRollno,txtVuStdClass;
    String stnVuStdName,stnVuStdEmail,stnVuStdPassword,stnVuStdRollno,stnVuStdClass;
    StudentDBMS studentDBMS;
    private Bundle bundle1,bundle2,bundle3,bundle4,bundle5;
    public StdInformationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_std_information, container, false);
        studentDBMS=new StudentDBMS(getContext());
        return view;

    }
}
