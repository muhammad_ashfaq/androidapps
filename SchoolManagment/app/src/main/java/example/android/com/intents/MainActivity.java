package example.android.com.intents;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView txtVuTeacher,txtVuStudent,txtVuLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtVuTeacher= (TextView) findViewById(R.id.txt_vu_teacher);
        txtVuStudent= (TextView) findViewById(R.id.txt_vu_student);
        txtVuLogin= (TextView) findViewById(R.id.txt_vu_login);
        txtVuTeacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent=new Intent(MainActivity.this,SignupTeacher.class);
                startActivity(mIntent);
            }
        });
        txtVuStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent=new Intent(MainActivity.this,SignupStudent.class);
                startActivity(mIntent);
            }
        });
        txtVuLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent=new Intent(MainActivity.this,LoginActivity.class);
                startActivity(mIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertBuilder=new AlertDialog.Builder(MainActivity.this);
        alertBuilder.setTitle("Exit");
        alertBuilder.setMessage("Are you sure to quit ?");
        alertBuilder.setPositiveButton("Yes",   new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        alertBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog alertDialog=alertBuilder.create();
        alertDialog.show();
    }
}









   /* Button btnIntent;
    Button btnIntentSend;*/
 /* btnIntent= (Button) findViewById(R.id.btn_intent);
        btnIntentSend =(Button) findViewById(R.id.btn_intent_share);
        *//*Gallary view to select a image.*//*
        btnIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent=new Intent();
                mIntent.setType("image*//*");
                mIntent.setAction(mIntent.ACTION_VIEW);
                startActivity(mIntent);
            }
        });
        btnIntentSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent=new Intent();
                mIntent.setAction(mIntent.ACTION_SEND);
                mIntent.putExtra(Intent.EXTRA_TITLE,"MegaApps");
                mIntent.putExtra(Intent.EXTRA_TEXT,"Enjoy the latest songs from our app.");
                mIntent.setType("text/plain");
                if(mIntent.resolveActivity(getPackageManager())!= null){
                    startActivity(mIntent);
                }

            }
        });

    }*/