package example.android.com.intents;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import example.android.com.intents.dbmspkg.StudentDBMS;

public class SignupStudent extends AppCompatActivity {
    TextInputEditText editTextStdFullName,editTextStdEmail,editTextStdPassword
            ,editTextStdClassName,editTextStdRollNo;
    String incomingStdFullName,incomingStdEmail, incomingStdPassword
            ,incomingStdClassName, incomingStdRollNo;
    TextView txtVuGetStarted;
    StudentDBMS dbmsStd;
    SQLiteDatabase sqLiteStdDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_student);
        editTextStdFullName=(TextInputEditText) findViewById(R.id.edt_txt_student_fullname);
        editTextStdEmail=(TextInputEditText) findViewById(R.id.edt_txt_student_email);
        editTextStdPassword=(TextInputEditText) findViewById(R.id.edt_txt_student_password);
        editTextStdClassName=(TextInputEditText) findViewById(R.id.edt_txt_student_classname);
        editTextStdRollNo=(TextInputEditText) findViewById(R.id.edt_txt_student_rollno);
        txtVuGetStarted=(TextView)findViewById(R.id.txt_vu_student_get_started);
        dbmsStd=new StudentDBMS(SignupStudent.this);


        txtVuGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                incomingStdFullName =editTextStdFullName.getText().toString();
                incomingStdEmail=editTextStdEmail.getText().toString();
                incomingStdPassword =editTextStdPassword.getText().toString();
                incomingStdClassName=editTextStdClassName.getText().toString();
                incomingStdRollNo =editTextStdRollNo.getText().toString();
                if(stdFormValidate()){
                    sqLiteStdDatabase=dbmsStd.getWritableDatabase();
                    dbmsStd.insertNewStudent(incomingStdFullName,incomingStdEmail,incomingStdPassword,incomingStdClassName,
                            incomingStdRollNo,sqLiteStdDatabase);
                    Intent mIntent=new Intent(SignupStudent.this,StudentHomeActivity.class);
                    mIntent.putExtra("stdname",incomingStdFullName);
                    mIntent.putExtra("stdemail",incomingStdEmail);
                    mIntent.putExtra("stdpassword",incomingStdPassword);
                    mIntent.putExtra("stdclass",incomingStdClassName);
                    mIntent.putExtra("stdrollno",incomingStdRollNo);
                    startActivity(mIntent);
                    Toast.makeText(SignupStudent.this, "Congrates", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });

    }
    public boolean stdFormValidate(){
        boolean valid=true;
        if(incomingStdFullName.isEmpty() || incomingStdFullName.length()>32){
            editTextStdFullName.setError("Please enter a valid Name");
            valid=false;
        }
        if(incomingStdEmail.isEmpty() ||  !Patterns.EMAIL_ADDRESS.matcher(incomingStdEmail).matches()){
            editTextStdEmail.setError("Please enter a valid Email");
            valid=false;
        }
        if(incomingStdPassword.isEmpty()){
            editTextStdPassword.setError("Please enter Password");
            valid=false;
        }
        if(incomingStdClassName.isEmpty()){
            editTextStdClassName.setError("Please enter your Class.");
            valid=false;
        }
        if(incomingStdRollNo.isEmpty()){
            editTextStdRollNo.setError("Please enter your Roll no");
            valid=false;
        }
        return valid;
    }
}
