package example.android.com.intents.dbmspkg;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by AndroidBuddy on 11/13/2017.
 */

public class StudentDBMS extends SQLiteOpenHelper {
    public static final String dbName="Userloginsystem.db";
    public static int dbVersion=1;
    public boolean dbStdStatus;
    public String tableName="stdDetails";

    private String createTableQuery="CREATE TABLE "+tableName+"(std_name TEXT,std_email TEXT," +
                                     "std_password TEXT,std_class_name,std_roll_no TEXT);";
    public StudentDBMS(Context context){
        super(context,dbName,null,dbVersion);
        Log.e("Constructer","Well done.DB created.");
    }
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
       sqLiteDatabase.execSQL(createTableQuery);
       Log.e("onCreate()===>","structure created.");
       dbStdStatus=true;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void insertNewStudent(String incomingStdFullName, String incomingStdEmail,
                                 String incomingStdPassword,
                                 String incomingStdClassName, String incomingStdRollNo, SQLiteDatabase sqLiteStdDatabase) {
        ContentValues cv=new ContentValues();
        cv.put("std_name",incomingStdFullName);
        cv.put("std_email",incomingStdEmail);
        cv.put("std_password",incomingStdPassword);
        cv.put("std_class_name",incomingStdClassName);
        cv.put("std_roll_no",incomingStdRollNo);
        sqLiteStdDatabase.insert("stdDetails",null,cv);
    }
    public boolean checkStudentLogin(String incomingEmail,String incomingPassword,SQLiteDatabase sqLiteDatabase){
        Cursor resultCursor;
        resultCursor=sqLiteDatabase.rawQuery("select std_email and std_password from stdDetails where std_email='"+incomingEmail+"' and std_password='"+incomingPassword+"'",null);
        if(resultCursor.getCount()==1) {
            return true;
        }else{
            return false;
        }
    }


}
