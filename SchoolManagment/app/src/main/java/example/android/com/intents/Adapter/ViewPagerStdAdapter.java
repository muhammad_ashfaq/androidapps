package example.android.com.intents.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by AndroidBuddy on 3/8/2018.
 */

public class ViewPagerStdAdapter extends FragmentPagerAdapter {
    ArrayList<Fragment> fragmentsStd=new ArrayList<>();
    ArrayList<String> tabTitlesStd=new ArrayList<>();
    ViewPagerStdAdapter viewPagerStdAdapter;

    public void addFragments(Fragment fragments,String tabTitles){
        this.fragmentsStd.add(fragments);
        this.tabTitlesStd.add(tabTitles);
    }
    public ViewPagerStdAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentsStd.get(position);
    }

    @Override
    public int getCount() {
        return fragmentsStd.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitlesStd.get(position);
    }
}
