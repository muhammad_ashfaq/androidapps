package example.android.com.intents.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by AndroidBuddy on 3/8/2018.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    ArrayList<Fragment> fragmentArrayList=new ArrayList<>();
    ArrayList<String> tabTitlesArrayList=new ArrayList<>();

    public void addFragments(Fragment fragments,String tabtitles){
        this.fragmentArrayList.add(fragments);
        this.tabTitlesArrayList.add(tabtitles);
    }
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentArrayList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentArrayList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitlesArrayList.get(position);
    }
}
