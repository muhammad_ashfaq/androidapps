package example.android.com.intents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import example.android.com.intents.dbmspkg.StudentDBMS;
import example.android.com.intents.dbmspkg.TeacherDBMS;

public class SplashScreen extends AppCompatActivity {
    /*LoginActivity loginActivity;
    StudentDBMS studentDBMS;
    TeacherDBMS teacherDBMS;*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_splash);
        /*loginActivity=new LoginActivity();
        studentDBMS=new StudentDBMS(this);
        teacherDBMS=new TeacherDBMS(this);*/


        splashScreen();
    }
    private  void splashScreen(){
        Thread mThread= new Thread(){
            public void run(){
                super.run();
                try {
                    Thread.sleep(2000);
                    Intent mIntent=new Intent(SplashScreen.this,MainActivity.class);
                    startActivity(mIntent);
                    finish();
                     /*if(studentDBMS.dbStdStatus=true){
                        Intent mIntent=new Intent(SplashScreen.this,MainActivity.class);
                        startActivity(mIntent);
                        finish();
                    }else if(teacherDBMS.dbTeacherStatus==true){
                        Intent mIntent=new Intent(SplashScreen.this,MainActivity.class);
                        startActivity(mIntent);
                        finish();
                    }else if(loginActivity.loginTeacherStatus==true){
                        Intent mIntent=new Intent(SplashScreen.this,TeacherHomeActivity.class);
                        startActivity(mIntent);
                        finish();
                    }else if(loginActivity.loginStdStatus==true){
                        Intent mIntent=new Intent(SplashScreen.this,StudentHomeActivity.class);
                        startActivity(mIntent);
                        finish();*/
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        };
        mThread.start();
    }
}
