package com.example.jackreacher.calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btnZero, btnOne, btnTwo, btnThree, btnFour, btnFive, btnSix, btnSeven, btnEight, btnNine, btnPlus, btnMinus, btnDivide;
    int varbtnZero, varbtnOne, varbtnTwo, varbtnThree, varbtnFour, varbtnFive, varbtnSix, varbtnSeven, varbtnEight, varbtnNine;
    String varbtnPlus, varbtnMinus,varbtnMultiply, varbtnDivide;
    Button btnClearAll, btnClearOneByOne,btnMultiply, btnEqual;
    EditText valueOne, operator, valueTwo;
    TextView answerTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnZero = (Button) findViewById(R.id.btn_zero);
        btnOne = (Button) findViewById(R.id.btn_one);
        btnTwo = (Button) findViewById(R.id.btn_two);
        btnThree = (Button) findViewById(R.id.btn_three);
        btnFour = (Button) findViewById(R.id.btn_four);
        btnFive = (Button) findViewById(R.id.btn_five);
        btnSix = (Button) findViewById(R.id.btn_six);
        btnSeven = (Button) findViewById(R.id.btn_seven);
        btnEight = (Button) findViewById(R.id.btn_eigth);
        btnNine = (Button) findViewById(R.id.btn_nine);
        btnPlus = (Button) findViewById(R.id.btn_add);
        btnMinus = (Button) findViewById(R.id.btn_subtract);
        btnMultiply = (Button) findViewById(R.id.btn_multiply);
        btnDivide = (Button) findViewById(R.id.btn_divide);
        btnClearAll = (Button) findViewById(R.id.btn_clear);
        btnClearOneByOne = (Button) findViewById(R.id.btn_clear_one_by_one);
        valueOne = (EditText) findViewById(R.id.value_one_edit_text);
        operator = (EditText) findViewById(R.id.operator_edit_text);
        valueTwo = (EditText) findViewById(R.id.value_two_edit_text);
        answerTextView = (TextView) findViewById(R.id.answer_text_view);

        btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String varbtnZero,varbtnOne,varbtnTwo,varbtnThree,varbtnFour,varbtnFive,varbtnSix,varbtnSeven,varbtnEight,varbtnNine,varbtnPlus,varbtnMinus,varbtnDivide;
                String one = btnOne.getText().toString();
                varbtnOne = Integer.parseInt(one);
            }
        });
        btnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String two = btnTwo.getText().toString();
                varbtnTwo = Integer.parseInt(two);
            }
        });
        btnThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String three = btnThree.getText().toString();
                varbtnThree = Integer.parseInt(three);
            }
        });
        btnFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String four = btnFour.getText().toString();
                varbtnFour = Integer.parseInt(four);
            }
        });
        btnFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String five = btnFive.getText().toString();
                varbtnFive = Integer.parseInt(five);
            }
        });
        btnSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String six = btnSix.getText().toString();
                varbtnSix = Integer.parseInt(six);
            }
        });
        btnSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String seven = btnSeven.getText().toString();
                varbtnSeven = Integer.parseInt(seven);
            }
        });
        btnEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String eight = btnEight.getText().toString();
                varbtnEight = Integer.parseInt(eight);
            }
        });
        btnNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nine = btnNine.getText().toString();
                varbtnNine = Integer.parseInt(nine);
            }
        });
        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                varbtnPlus=btnPlus.getText().toString();
            }
        });
        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                varbtnMinus=btnMinus.getText().toString();
            }
        });
        btnMultiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                varbtnMultiply=btnMultiply.getText().toString();
            }
        });
        btnDivide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                varbtnDivide=btnDivide.getText().toString();
            }
        });

        if(varbtnPlus== null | varbtnMinus == null|| varbtnMultiply==null || varbtnDivide==null){
            valueOne.setText(varbtnZero);
            valueOne.setText(varbtnOne);
            valueOne.setText(varbtnTwo);
            valueOne.setText(varbtnThree);
            valueOne.setText(varbtnFour);
            valueOne.setText(varbtnFive);
            valueOne.setText(varbtnSix);
            valueOne.setText(varbtnSeven);
            valueOne.setText(varbtnEight);
            valueOne.setText(varbtnNine);
            }
            else if(varbtnPlus=="+"){
            operator.setText(varbtnPlus);
        }
        else if(varbtnMinus=="-"){
            operator.setText(varbtnMinus);
        }else if(varbtnMultiply=="*"){
            operator.setText(varbtnMultiply);
        }else if(varbtnDivide == "/"){
            operator.setText(varbtnDivide);
        }else{
            valueTwo.setText(varbtnOne);
            valueTwo.setText(varbtnOne);
            valueTwo.setText(varbtnTwo);
            valueTwo.setText(varbtnThree);
            valueTwo.setText(varbtnFour);
            valueTwo.setText(varbtnFive);
            valueTwo.setText(varbtnSix);
            valueTwo.setText(varbtnSeven);
            valueTwo.setText(varbtnEight);
            valueTwo.setText(varbtnNine);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (R.id.optn_menu_privacy == id) {
            Toast.makeText(this, "Priacy", Toast.LENGTH_SHORT).show();
        } else {
            Intent mIntent = new Intent(MainActivity.this, About_Us_Activity.class);
            mIntent.putExtra("company_name", "MegaApps");
            startActivity(mIntent);
        }
        return true;
    }
}
