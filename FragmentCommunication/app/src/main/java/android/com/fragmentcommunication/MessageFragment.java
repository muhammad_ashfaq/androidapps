package android.com.fragmentcommunication;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment {
    private EditText editText;
    private Button btn;

    public MessageFragment() {
        // Required empty public constructor
    }
    public interface OnMessageReadListner{

        public void  OnReadMessage(String message);
    }

    OnMessageReadListner onMessageReadListner;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        editText = (EditText) view.findViewById(R.id.edit_txt_message);
        btn = (Button) view.findViewById(R.id.btn_send_message);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message=editText.getText().toString();
                onMessageReadListner.OnReadMessage(message);
            }
         }
        );
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity=(Activity)context;
        try{
            onMessageReadListner=(OnMessageReadListner)activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+"must override on message read");
        }
    }
}
