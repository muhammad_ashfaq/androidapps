package example.android.com.dailoging;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    Button btnProgDlg;
    Button btnCustomDlg;
    CustomDailog customDailog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCustomDlg = (Button) findViewById(R.id.btn_custom_progress_dlg);
        btnProgDlg = (Button) findViewById(R.id.btn_progress_dlg);
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Please wait a minute");
        progressDialog.setTitle("Uploading");

        customDailog = new CustomDailog(MainActivity.this);


        btnProgDlg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
            }
        });
        btnCustomDlg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDailog.show();
            }
        });

    }
    //MySubClass for the android dialogue of life!
    private class CustomDailog extends Dialog  {
        Button btnTestDlg;
        ImageView imageView;

        public CustomDailog(@NonNull Context context) {
            super(context);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.custom_dlg_layout);
            btnTestDlg = (Button) findViewById(R.id.btn_test_dlg);
            imageView = (ImageView)findViewById(R.id.image_view);

/*
            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
*/
            btnTestDlg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(MainActivity.this, "Custom Dailog", Toast.LENGTH_SHORT).show();
            Picasso.with(MainActivity.this).load("https://megaapps.000webhost.com/images/logo_inkspace.png").into(imageView);
                }
            });
        }
    }
}
