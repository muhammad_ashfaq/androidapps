package example.android.com.youtube.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import example.android.com.youtube.R;

/**
 * Created by JackReacher on 10/14/2017.
 */

public class MyAdapter extends ArrayAdapter {
    int[] videoThumbArray,channelPicArray;
    int resource;
    Context context;
    String[] videoTitleArray,channelNameArray,videoLikesArray,videoUploadtimeArray;
    LayoutInflater mLayoutInflater;


    public MyAdapter(@NonNull Context context, @LayoutRes int resource, int[] videoThumbArray, int[] channelPicArray,
                     String[] videoTitleArray,String[] channelNameArray, String[] videoLikesArray, String[] videoUploadtimeArray) {
        super(context, resource);
        this.context=context;
        this.resource=resource;
        this.videoThumbArray=videoThumbArray;
        this.channelPicArray=channelPicArray;
        this.videoTitleArray = videoTitleArray;
        this.channelNameArray=channelNameArray;
        this.videoLikesArray=videoLikesArray;
        this.videoUploadtimeArray=videoUploadtimeArray;
        mLayoutInflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return videoThumbArray.length;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView=mLayoutInflater.inflate(R.layout.youtube_custom_design,null);

        ImageView videoThumb=(ImageView)convertView.findViewById(R.id.image_video_thumb);
        ImageView channelPic=(ImageView)convertView.findViewById(R.id.image_pic_channel);
        TextView videoTitle=(TextView)convertView.findViewById(R.id.txt_vu_video_title);
        TextView channelName=(TextView)convertView.findViewById(R.id.txt_vu_channel_name);
        TextView videoLikes=(TextView) convertView.findViewById(R.id.txt_vu_video_likes);
        TextView videoUploadtime=(TextView)convertView.findViewById(R.id.txt_vu_video_upload_time);

        videoThumb.setImageResource(videoThumbArray[position]);
        channelPic.setImageResource(channelPicArray[position]);
        videoTitle.setText(videoTitleArray[position]);
        channelName.setText(channelNameArray[position]);
        videoLikes.setText(videoLikesArray[position]);
        videoUploadtime.setText(videoUploadtimeArray[position]);
        return convertView;
    }
}
