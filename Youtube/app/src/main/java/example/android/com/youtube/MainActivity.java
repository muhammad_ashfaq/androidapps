package example.android.com.youtube;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.zip.Inflater;

import example.android.com.youtube.Adapter.MyAdapter;

public class MainActivity extends AppCompatActivity {
    ListView lstVuYoutube;
    LayoutInflater mLayoutInflater;

    int[] videoThumbArray =
            {R.drawable.pic_1, R.drawable.pic_2, R.drawable.pic_3, R.drawable.pic_4, R.drawable.pic_5,
            R.drawable.pic_6,R.drawable.pic_7,R.drawable.pic_8,R.drawable.pic_9,R.drawable.pic_10,R.drawable.pic_11,
            R.drawable.pic_12};

    int[] channelPicArray =
            {R.drawable.channel_pic, R.drawable.channel_pic, R.drawable.channel_pic,
            R.drawable.channel_pic, R.drawable.channel_pic, R.drawable.channel_pic,
            R.drawable.channel_pic, R.drawable.channel_pic, R.drawable.channel_pic,
            R.drawable.channel_pic, R.drawable.channel_pic, R.drawable.channel_pic};

    String[] videoTitleArray =
            {"Sochenge tumhe pyar kare k nahi-Youtube", "Channa Meraya-Unpluuged by Ranbir Kappor-Youtube",
            "Tere Jaisa Yaar Kahan Unplugged cover", "Atif Aslam Hum Kuy Chalen -Youtube",
            "Pyar ki ek Kahani-Bluray Karish Song.mp4 -Youtube", "Kaisay jien gain kaisay Atif Aslam Song",
            "Tere Mere Song(Respire)|Feat.Armaan Malik|latest -Youtube","KabhiYadon mn Aao Kabhi Khawbon mn aao Arjit Singh","Sochta Hoon-Official Remix 2017",
            "Ek Mera Yaar Ek odi Yaari| Khair Mangda by Atif Aslam -Youtube","Dukhry Kol Kol By Quratulain Balaoch -Youtube","Khaab - Akhil New Panjabi Song Feat Permesh Verma"};
    String[] channelNameArray = {"T-series", "Zee Music Company", "AwesomeSongs", "Atif Aslam fans", "T-series", "Tune Layrico","T-series","MicroboxGift"
    ,"Hi-Tech-Music Ltd","Rising Stars","Personal Thoughts","iAm Awesome"};
    String[] videoLikesArray = {"500k views", "4M views", "2M views", "900k views", "3.3M views", "658k views","2.1M view",
    "16M viws","847k views","187k views","34k views","23M views"};
    String[] videoUploadtimeArray = {"2 months ago", "4 months ago", "1 month ago", "5 months ago", "1 year ago", "8 months ago","2 day ago"
    ,"18 months ago","1 month ago","8 months ago","1 month ago","1 year ago"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lstVuYoutube = (ListView) findViewById(R.id.lst_vu_youtube);
        ArrayAdapter myArrayAdapter = new MyAdapter(MainActivity.this, R.layout.youtube_custom_design, videoThumbArray, channelPicArray
                , videoTitleArray, channelNameArray, videoLikesArray, videoUploadtimeArray);
            lstVuYoutube.setAdapter(myArrayAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_youtube_file, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.setting_menu:
                Intent mSettingIntent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(mSettingIntent);
                break;
            case R.id.signin_menu:
                Intent mSigninIntent = new Intent(MainActivity.this, SigninActivity.class);
                startActivity(mSigninIntent);
        }
        return true;
    }


}