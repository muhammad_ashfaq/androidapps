package example.android.com.databasepractice;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by JackReacher on 9/29/2017.
 */

public class DBMS extends SQLiteOpenHelper {
    public static final String DBName="SqliteDatabase";
    public static int DBVersion=1;
    public String createTableQuery="CREATE TABLE userDetails(user_name TEXT,user_email TEXT,user_password TEXT,user_CNIC INT);";

    public DBMS(Context context){
        super(context,DBName,null,DBVersion);
        Log.e("Database operations","Database created.");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(createTableQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
